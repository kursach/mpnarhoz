﻿namespace MediaPlayer
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pl = new System.Windows.Forms.DataGridView();
            this.Nam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tim = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Location = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OpenDlg = new System.Windows.Forms.OpenFileDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.PosBar = new System.Windows.Forms.TrackBar();
            this.PanBar = new System.Windows.Forms.TrackBar();
            this.VolBar = new System.Windows.Forms.TrackBar();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.NextBtn = new System.Windows.Forms.Button();
            this.StopBtn = new System.Windows.Forms.Button();
            this.PauseBtn = new System.Windows.Forms.Button();
            this.PlayBtn = new System.Windows.Forms.Button();
            this.PrevBtn = new System.Windows.Forms.Button();
            this.DelBtn = new System.Windows.Forms.Button();
            this.Spectrum = new System.Windows.Forms.PictureBox();
            this.AddFile = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PosBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VolBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Spectrum)).BeginInit();
            this.SuspendLayout();
            // 
            // pl
            // 
            this.pl.AllowUserToAddRows = false;
            this.pl.AllowUserToResizeColumns = false;
            this.pl.AllowUserToResizeRows = false;
            this.pl.BackgroundColor = System.Drawing.SystemColors.InactiveCaption;
            this.pl.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Red;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.pl.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.pl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.pl.ColumnHeadersVisible = false;
            this.pl.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nam,
            this.Tim,
            this.Location});
            this.pl.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.pl.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pl.Location = new System.Drawing.Point(2, 122);
            this.pl.MultiSelect = false;
            this.pl.Name = "pl";
            this.pl.ReadOnly = true;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Red;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.pl.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.pl.RowHeadersVisible = false;
            this.pl.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.pl.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.pl.RowTemplate.ReadOnly = true;
            this.pl.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.pl.Size = new System.Drawing.Size(324, 338);
            this.pl.TabIndex = 0;
            this.pl.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.pl_CellClick);
            this.pl.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.pl_CellDoubleClick);
            this.pl.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.pl_CellEnter);
            // 
            // Nam
            // 
            this.Nam.HeaderText = "nam";
            this.Nam.Name = "Nam";
            this.Nam.ReadOnly = true;
            this.Nam.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Nam.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Nam.Width = 250;
            // 
            // Tim
            // 
            this.Tim.HeaderText = "tim";
            this.Tim.Name = "Tim";
            this.Tim.ReadOnly = true;
            this.Tim.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Tim.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Tim.Width = 70;
            // 
            // Location
            // 
            this.Location.HeaderText = "loc";
            this.Location.Name = "Location";
            this.Location.ReadOnly = true;
            this.Location.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Location.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Location.Visible = false;
            // 
            // OpenDlg
            // 
            this.OpenDlg.Multiselect = true;
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // PosBar
            // 
            this.PosBar.AutoSize = false;
            this.PosBar.Cursor = System.Windows.Forms.Cursors.Default;
            this.PosBar.Location = new System.Drawing.Point(2, 71);
            this.PosBar.Margin = new System.Windows.Forms.Padding(0);
            this.PosBar.Maximum = 100;
            this.PosBar.Name = "PosBar";
            this.PosBar.Size = new System.Drawing.Size(324, 23);
            this.PosBar.TabIndex = 8;
            this.PosBar.TabStop = false;
            this.PosBar.TickStyle = System.Windows.Forms.TickStyle.None;
            this.PosBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PosBar_MouseDown);
            this.PosBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PosBar_MouseUp);
            // 
            // PanBar
            // 
            this.PanBar.AutoSize = false;
            this.PanBar.Location = new System.Drawing.Point(296, 2);
            this.PanBar.Margin = new System.Windows.Forms.Padding(0);
            this.PanBar.Maximum = 100;
            this.PanBar.Minimum = -100;
            this.PanBar.Name = "PanBar";
            this.PanBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.PanBar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PanBar.Size = new System.Drawing.Size(30, 69);
            this.PanBar.TabIndex = 9;
            this.PanBar.TickFrequency = 20;
            this.PanBar.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.PanBar.Scroll += new System.EventHandler(this.PanBar_Scroll);
            // 
            // VolBar
            // 
            this.VolBar.AutoSize = false;
            this.VolBar.Location = new System.Drawing.Point(2, 2);
            this.VolBar.Margin = new System.Windows.Forms.Padding(0);
            this.VolBar.Maximum = 100;
            this.VolBar.Name = "VolBar";
            this.VolBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.VolBar.Size = new System.Drawing.Size(30, 69);
            this.VolBar.TabIndex = 10;
            this.VolBar.TickFrequency = 10;
            this.VolBar.Value = 100;
            this.VolBar.Scroll += new System.EventHandler(this.VolBar_Scroll);
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // NextBtn
            // 
            this.NextBtn.Image = global::MediaPlayer.Properties.Resources.Forward;
            this.NextBtn.Location = new System.Drawing.Point(266, 97);
            this.NextBtn.Name = "NextBtn";
            this.NextBtn.Size = new System.Drawing.Size(60, 22);
            this.NextBtn.TabIndex = 5;
            this.NextBtn.UseVisualStyleBackColor = true;
            this.NextBtn.Click += new System.EventHandler(this.NextBtn_Click);
            // 
            // StopBtn
            // 
            this.StopBtn.Image = global::MediaPlayer.Properties.Resources.Stop__2_;
            this.StopBtn.Location = new System.Drawing.Point(200, 97);
            this.StopBtn.Name = "StopBtn";
            this.StopBtn.Size = new System.Drawing.Size(60, 22);
            this.StopBtn.TabIndex = 4;
            this.StopBtn.UseVisualStyleBackColor = true;
            this.StopBtn.Click += new System.EventHandler(this.StopBtn_Click);
            // 
            // PauseBtn
            // 
            this.PauseBtn.Image = global::MediaPlayer.Properties.Resources.Pause;
            this.PauseBtn.Location = new System.Drawing.Point(134, 97);
            this.PauseBtn.Name = "PauseBtn";
            this.PauseBtn.Size = new System.Drawing.Size(60, 22);
            this.PauseBtn.TabIndex = 3;
            this.PauseBtn.UseVisualStyleBackColor = true;
            this.PauseBtn.Click += new System.EventHandler(this.PauseBtn_Click);
            // 
            // PlayBtn
            // 
            this.PlayBtn.Image = global::MediaPlayer.Properties.Resources.Play;
            this.PlayBtn.Location = new System.Drawing.Point(68, 97);
            this.PlayBtn.Name = "PlayBtn";
            this.PlayBtn.Size = new System.Drawing.Size(60, 22);
            this.PlayBtn.TabIndex = 2;
            this.PlayBtn.UseVisualStyleBackColor = true;
            this.PlayBtn.Click += new System.EventHandler(this.PlayBtn_Click);
            // 
            // PrevBtn
            // 
            this.PrevBtn.Image = global::MediaPlayer.Properties.Resources.Rewind;
            this.PrevBtn.Location = new System.Drawing.Point(2, 97);
            this.PrevBtn.Name = "PrevBtn";
            this.PrevBtn.Size = new System.Drawing.Size(60, 22);
            this.PrevBtn.TabIndex = 1;
            this.PrevBtn.UseVisualStyleBackColor = true;
            this.PrevBtn.Click += new System.EventHandler(this.PrevBtn_Click);
            // 
            // DelBtn
            // 
            this.DelBtn.Image = global::MediaPlayer.Properties.Resources.X2;
            this.DelBtn.Location = new System.Drawing.Point(48, 463);
            this.DelBtn.Name = "DelBtn";
            this.DelBtn.Size = new System.Drawing.Size(40, 27);
            this.DelBtn.TabIndex = 13;
            this.DelBtn.UseVisualStyleBackColor = true;
            this.DelBtn.Click += new System.EventHandler(this.DelBtn_Click);
            // 
            // Spectrum
            // 
            this.Spectrum.Location = new System.Drawing.Point(35, 12);
            this.Spectrum.Name = "Spectrum";
            this.Spectrum.Size = new System.Drawing.Size(260, 56);
            this.Spectrum.TabIndex = 12;
            this.Spectrum.TabStop = false;
            // 
            // AddFile
            // 
            this.AddFile.Image = global::MediaPlayer.Properties.Resources.Plus;
            this.AddFile.Location = new System.Drawing.Point(2, 463);
            this.AddFile.Name = "AddFile";
            this.AddFile.Size = new System.Drawing.Size(40, 27);
            this.AddFile.TabIndex = 7;
            this.AddFile.UseVisualStyleBackColor = true;
            this.AddFile.Click += new System.EventHandler(this.AddFile_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(328, 492);
            this.Controls.Add(this.DelBtn);
            this.Controls.Add(this.Spectrum);
            this.Controls.Add(this.VolBar);
            this.Controls.Add(this.PanBar);
            this.Controls.Add(this.PosBar);
            this.Controls.Add(this.AddFile);
            this.Controls.Add(this.NextBtn);
            this.Controls.Add(this.StopBtn);
            this.Controls.Add(this.PauseBtn);
            this.Controls.Add(this.PlayBtn);
            this.Controls.Add(this.PrevBtn);
            this.Controls.Add(this.pl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PosBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VolBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Spectrum)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView pl;
        private System.Windows.Forms.Button PrevBtn;
        private System.Windows.Forms.Button PauseBtn;
        private System.Windows.Forms.Button StopBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nam;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tim;
        private System.Windows.Forms.DataGridViewTextBoxColumn Location;
        public System.Windows.Forms.Button AddFile;
        public System.Windows.Forms.Button PlayBtn;
        public System.Windows.Forms.TrackBar PosBar;
        private System.Windows.Forms.TrackBar PanBar;
        public System.Windows.Forms.TrackBar VolBar;
        private System.Windows.Forms.PictureBox Spectrum;
        public System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.Timer timer2;
        public System.Windows.Forms.OpenFileDialog OpenDlg;
        public System.Windows.Forms.Button NextBtn;
        private System.Windows.Forms.Button DelBtn;
    }
}

