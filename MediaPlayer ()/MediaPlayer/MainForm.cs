﻿using System;

using System.Drawing;

using System.Windows.Forms;

using Un4seen.Bass;


namespace MediaPlayer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            BassNet.Registration("Dialog108@gmail.com", "2X9293019152222");
            InitializeComponent();
            EasyBass.InitBass(44100);

        }

        private void pl_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            pl.Rows[e.RowIndex].Selected = true;
        }

        private void pl_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            pl.Rows[e.RowIndex].Selected = true;
        }

        private void pl_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            pl.Rows[e.RowIndex].Selected = true;
            PlayBtn_Click(null, null);
        }

        private void AddFile_Click(object sender, EventArgs e)
        {

            if (OpenDlg.ShowDialog() == DialogResult.OK)
            {
                string fileName = OpenDlg.FileName;
                EasyTags Tags = new EasyTags(fileName);
                var ts = TimeSpan.FromSeconds(Tags.Length);
                string trackLength;
                if (ts.Seconds > 9)
                    trackLength = ts.Minutes.ToString() + ":" + ts.Seconds.ToString();
                else
                    trackLength = ts.Minutes.ToString() + ":" + "0" + ts.Seconds.ToString();

                pl.Rows.Add(Tags.Artist + " - " + Tags.Title, trackLength, OpenDlg.FileName);
            }
        }

        private void PlayBtn_Click(object sender, EventArgs e)
        {
            if (pl.CurrentRow != null)
            {
                int index = pl.CurrentRow.Index;
                string name = pl[2, index].Value.ToString();
                EasyBass.Play(name, VolBar.Value);
                timer1.Enabled = true;
                timer2.Enabled = true;
            }
            else
            {
                AddFile_Click(null, null);
                if (pl.RowCount > 0)
                {
                    int index = pl.CurrentRow.Index;
                    string name = pl[2, index].Value.ToString();
                    EasyBass.Play(name, VolBar.Value);
                    timer1.Enabled = true;
                    timer2.Enabled = true;
                }
            }
            PosBar.Maximum = EasyBass.GetTimeOfStream(EasyBass.Stream);
        }

        private void timer1_Tick(object sender, EventArgs e)  //Рисование спектра музыки
        {

            EasyBass.InitBmp(Spectrum.Width, Spectrum.Height);
            Spectrum.Image = EasyBass.Bmp;
            
            int h = Spectrum.Height - 1;
            Graphics g = Graphics.FromImage(Spectrum.Image);
            EasyBass.Spectr();  //получаем массив метаданных 
            int h1;
            for (int i = 0; i < 26; i++)
            {

                if (i>3)
                    h1 = (int) ((h + 200) * EasyBass.Fft[i]);
                else
                    h1 = (int)((h + 20) * EasyBass.Fft[i]);
                if (h1 > h)
                    h1 = h;
                g.DrawRectangle(EasyBass.Penc, 10 * i, h - h1, 8, h1);
            }
            GC.Collect();

        }

        private void PauseBtn_Click(object sender, EventArgs e)
        {
            if (EasyBass.Pause())
            {
                timer1.Enabled = false;
                timer2.Enabled = false;
            }
            else
            {
                timer1.Enabled = true;
                timer2.Enabled = true;
            }
        }

        private void VolBar_Scroll(object sender, EventArgs e)
        {
            EasyBass.SetVolume(VolBar.Value);
        }

        private void PanBar_Scroll(object sender, EventArgs e)
        {
            if ((-15 < PanBar.Value) && (15 > PanBar.Value))  //Прилипание к центру ползунка
                PanBar.Value = 0;
            EasyBass.SetBalance(PanBar.Value);
        }


        private void PosBar_MouseUp(object sender, MouseEventArgs e)
        {
            EasyBass.SetPos(EasyBass.Stream,PosBar.Value);
            timer2.Enabled = true;
        }

        private void PosBar_MouseDown(object sender, MouseEventArgs e)
        {
            timer2.Enabled = false;
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (EasyBass.IsPlay())
                PosBar.Value = EasyBass.GetPosOfStream(EasyBass.Stream);
            if (EasyBass.GetTimeOfStream(EasyBass.Stream) == PosBar.Value) //EasyBass.GetPosOfStream(EasyBass.Stream))
            {
                PosBar.Value = 0;
                int index = pl.CurrentRow.Index;
                if (index == pl.RowCount - 1)
                {
                    pl.Rows[index].Selected = true;
                    pl.CurrentCell = pl[0, 0];
                    StopBtn_Click(null, null);
                }
                else
                {
                    NextBtn_Click(null, null);
                }

            }
        }

        private void NextBtn_Click(object sender, EventArgs e)
        {
            {
                if (pl.CurrentRow != null)
                {
                    int index = pl.CurrentRow.Index;
                    if (index == pl.RowCount-1)
                    {
                        if (pl.Rows.Count > 1)
                        {
                            pl.Rows[index].Selected = true;
                            pl.CurrentCell = pl[0, 0];
                            if (EasyBass.IsPlay())
                            {
                                PlayBtn_Click(null, null);
                            }
                        }
                    }
                    else
                    {
                        pl.Rows[index].Selected = true;
                        pl.CurrentCell = pl[0, index + 1];
                        if (EasyBass.IsPlay())
                        {
                            PlayBtn_Click(null, null);
                        }
                    }
                }
            }

        }

        private void PrevBtn_Click(object sender, EventArgs e)
        {
            {
                if (pl.CurrentRow != null)
                {
                    int index = pl.CurrentRow.Index;
                    if (index == 0)
                    {
                        if (pl.Rows.Count > 1)
                        {
                            pl.Rows[index].Selected = true;
                            pl.CurrentCell = pl[0, pl.RowCount-1];
                            if (EasyBass.IsPlay())
                            {
                                PlayBtn_Click(null, null);
                            }
                        }
                    }
                    else
                    {
                        pl.Rows[index].Selected = true;
                        pl.CurrentCell = pl[0, index - 1];
                        if (EasyBass.IsPlay())
                        {
                            PlayBtn_Click(null, null);
                        }
                    }
                }
            }

        }

        private void StopBtn_Click(object sender, EventArgs e)
        {
            timer2.Enabled = false;
            timer1.Enabled = false;
            EasyBass.Stop();
            PosBar.Value = 0;
            Spectrum.Image = null;
            GC.Collect();
        }

        private void DelBtn_Click(object sender, EventArgs e)
        {
            if (pl.CurrentRow != null)
            {
                int index = pl.CurrentRow.Index;
                pl.Rows.RemoveAt(index);
            }
        }
    }
}
