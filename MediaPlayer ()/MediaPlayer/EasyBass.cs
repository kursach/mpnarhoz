﻿// seven

using System;

using System.Drawing;

using Un4seen.Bass;

namespace MediaPlayer
{
    public static class EasyBass
    {
        private static int HZ = 44100;
        public static bool InitDefaultDevice; //состояние инициализации библиотеки
        public static int Stream;
        public static int Volume = 100;
        public static float[] Fft = new float[128];

        public static Bitmap Bmp;
        public static Pen Penc;

        //Инициализация
        public static bool InitBass(int hz)
        {
            if (!InitDefaultDevice)
                InitDefaultDevice = Bass.BASS_Init(-1, hz, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);
            return InitDefaultDevice;
        }

        //Стоп
        public static void Stop()
        {
            Bass.BASS_ChannelStop(Stream);
            Bass.BASS_StreamFree(Stream);
        }
        //Воспроизвести
        public static void Play(string fileName, int vol)
        {
            if (InitBass(HZ))
            {
                if (Bass.BASS_ChannelIsActive(Stream) != BASSActive.BASS_ACTIVE_PAUSED)
                {
                    Stop();
                    Volume = vol;
                    Stream = Bass.BASS_StreamCreateFile(fileName, 0, 0, BASSFlag.BASS_DEFAULT);
                    if (Stream != 0)
                    {
                        Bass.BASS_ChannelSetAttribute(Stream, BASSAttribute.BASS_ATTRIB_VOL, Volume / 100F);
                        Bass.BASS_ChannelPlay(Stream, false);
                    }
                }
                else
                {
                    Bass.BASS_ChannelPlay(Stream, false);
                }  
            }
        }

        //Длительность файла (канала)
        public static int GetTimeOfStream(int stream)
        {
            long timeBytes = Bass.BASS_ChannelGetLength(stream);
            double time = Bass.BASS_ChannelBytes2Seconds(stream, timeBytes);
            return (int) time;
        }

        //Текущая позиция
        public static int GetPosOfStream(int stream)
        {
            long pos = Bass.BASS_ChannelGetPosition(stream);
            double posSec = Bass.BASS_ChannelBytes2Seconds(stream, pos);
            return (int) posSec;
        }

        //Перемотка
        public static void SetPos(int stream, int pos)
        {
            Bass.BASS_ChannelSetPosition(stream, (double) pos);
        }
        //Громкость
        public static void SetVolume(int vol)
        {
            Volume = vol;
            Bass.BASS_ChannelSetAttribute(Stream, BASSAttribute.BASS_ATTRIB_VOL, Volume/100F);
        }

        public static void SetBalance(int bal)
        {
            Bass.BASS_ChannelSetAttribute(Stream, BASSAttribute.BASS_ATTRIB_PAN, bal/100F);
        }
        //Пауза
        public static bool Pause()
        {
            if (Bass.BASS_ChannelIsActive(Stream) == BASSActive.BASS_ACTIVE_PLAYING)
            {
                Bass.BASS_ChannelPause(Stream);
                return true;
            }
            else
            {
                Bass.BASS_ChannelPlay(Stream, false);
                return false;
            }
        }

        public static void Spectr()
        {
            Bass.BASS_ChannelGetData(Stream, Fft, (int)(BASSData.BASS_DATA_AVAILABLE | BASSData.BASS_DATA_FFT256));
        }

        public static void InitBmp(int width, int height)
        {
            Bmp = new Bitmap(width, height);
            Penc = new Pen(Color.LightSteelBlue, .5f);

        }

        public static bool IsPlay()
        {
            if (Bass.BASS_ChannelIsActive(Stream) == BASSActive.BASS_ACTIVE_PLAYING)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
